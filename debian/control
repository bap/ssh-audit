Source: ssh-audit
Section: admin
Priority: optional
Maintainer: ChangZhuo Chen (陳昌倬) <czchen@debian.org>
Build-Depends: debhelper-compat (= 13),
               asciidoc-base,
               dh-exec,
               dh-python,
	       python3-all,
	       python3-setuptools,
               python3,
               xmlto,
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://github.com/jtesta/ssh-audit
Vcs-Git: https://salsa.debian.org/debian/ssh-audit.git
Vcs-Browser: https://salsa.debian.org/debian/ssh-audit

Package: ssh-audit
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         ${python3:Depends},
Description: tool for ssh server auditing
 ssh-audit is a tool for ssh server auditing with the following
 features:
 .
  * SSH1 and SSH2 protocol server support;
  * grab banner, recognize device or software and operating system,
    detect compression;
  * gather key-exchange, host-key, encryption and message authentication
    code algorithms;
  * output algorithm information (available since, removed/disabled,
    unsafe/weak/legacy, etc);
  * output algorithm recommendations (append or remove based on
    recognized software version);
  * output security information (related issues, assigned CVE list,
    etc);
  * analyze SSH version compatibility based on algorithm information;
  * historical information from OpenSSH, Dropbear SSH and libssh;
  * no dependencies, compatible with Python 2.6+, Python 3.x and PyPy;
